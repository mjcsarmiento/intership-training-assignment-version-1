"""
Miracleo Jean C. Sarmiento
BS in Computer Science
Ateneo de Davao University
"""

from datetime import datetime # This is to import datetime to easily compute elapsed time of the game.
from operator import attrgetter # This is to sort the values in the attributes: time, errors, username
from os.path import isfile # This is to check if there is no existing file so that it can create and be read.
from random import randint # This is to generate random number N in randint(a, b) wherein a <= N <= b


class User(object):
    def __init__(self, time="", errors="", username=""):
        self.time = time
        self.errors = errors
        self.username = username


def add_or_subtract(choice):
    """
    This function generates 2 2-digit numbers (first_number & second_number) wherein based on the
    generated choice, it will add or subtract the 2 generated numbers. Afterwhich, it checks if
    user's answer is correct and has its corresponding return value.
    Args:
        choice: (int) This will accept 1 or 2 based on the conditions set in function 'questions'.

    Returns:
        Boolean value: The return value is True when user's answer is correct, False if otherwise.
    """

    first_number = randint(10, 99)
    second_number = randint(10, 99)

    if (choice == 1):
        correct_answer = first_number + second_number
        user_answer = raw_input("%s + %s = " % (str(first_number), str(second_number)))
    else:
        """
        In subtraction part, it make sures that the output will never be a negative number
        by making sure that the minuend has bigger value than subtrahend.
        """
        if (first_number > second_number):
            correct_answer = first_number - second_number
            user_answer = raw_input("%s - %s = " % (str(first_number), str(second_number)))
        else:
            correct_answer = second_number - first_number
            user_answer = raw_input("%s - %s = " % (str(second_number), str(first_number)))

    if (str(correct_answer) == user_answer):
        return True
    else:
        return False


def multiply():
    """
    This function generates a 2-digit number (first_number) and a 1-digit number except 0 and 1 (second_number)
    and it will multiply the 2 generated numbers. Afterwhich, it checks if user's answer is correct
    and has its corresponding return value.

    Returns:
        Boolean value: The return value is True when user's answer is correct, False if otherwise.
    """


    first_number = randint(10,99)
    second_number = randint(2,9)

    correct_answer = first_number * second_number
    user_answer = raw_input("%s * %s = " % (str(first_number), str(second_number)))

    if (str(correct_answer) == user_answer):
       return True
    else:
       return False


def divide():
    """
    This function generates a 3-digit number (first_number) and a 1-digit number except 0 and 1 (second_number).
    and it will divide the 2 generated numbers. Afterwhich, it checks if user's answer is correct
    and has its corresponding return value.

    Returns:
        Boolean value: The return value is True when user's answer is correct, False if otherwise.
    """

    first_number = randint(100, 999)
    second_number = randint(2, 9)

    """
    If first_number is not divisible by second_number, its remainder will be subtracted from first number
    so that the remainder now becomes zero (which means first_number is now divisible by second_number).
    """
    if (first_number%second_number != 0):
        first_number  -= (first_number%second_number)

    correct_answer = first_number / second_number
    user_answer = raw_input("%s / %s = " % (str(first_number), str(second_number)))

    if (str(correct_answer) == user_answer):
        return True
    else:
        return False


def questions(points, errors):
    """
    This is a recursive function wherein it keeps on operating unless user reaches 10 points or 10 errors.
    When finally terminated, it has its corresponding return value.

    Args:
        points (int): This value will let the function terminate if it reaches 10.
        errors (int): This value will let the function terminate if it reaches 10.

    Returns:
        Return value for recursion and terminates if terminating condition is satisfied.
        int value that helps determine if user passed or failed
    """

    if (points == 10 or errors == 10):
        return errors
    else:
        # This is help randomize the operation to be answered by user.
        choice = randint(1,4)

        if (choice == 3):
            operation = multiply()
        elif (choice == 4):
            operation = divide()
        else:
            operation = add_or_subtract(choice)

        if operation:
            print "CORRECT! You earn 1 point."
            return questions(points + 1, errors)
        else:
            print "WRONG! Try again."
            return questions(points, errors + 1)


def timer(start_time, end_time):
    """
    This function simply computes the time.

    Args:
        start_time (datetime): This value is the start time of the function.
        end_time (datetime): This value is the end time of the functions.

    Returns:
        String value which includes minutes and seconds (rounded off).
    """
    
    elapsed_time = end_time - start_time
    hours, minutes, seconds = str(elapsed_time).split(':')

    # This returns a ##:## format wher # is a 1-digit number
    return "%s:%02.0f" % (minutes, round(float(seconds)))


def get_list_of_players():
    """
    This function gets the original list from scores.txt.

    Returns:
        A list of objects with attributes time, errors and username
    """

    # If there is no existing file scores.txt, it creates the said file.
    if not isfile("scores.txt"):
        write = open("scores.txt", "w")
        write.close()

    read_file = open("scores.txt", "r")
    user_classes = list()

    # This gets each player and their results so that it will be appended to a list of User objects
    for player in read_file.readlines():
        temp_list = player.strip("\n").split("\t")
        user = User(temp_list[0], temp_list[1], temp_list[2])
        user_classes.append(user)

    read_file.close()
    return user_classes


def if_passed(errors_by_user, list_record, current_instance):
    """
    This function gets the number of errors according to the results of the current user.
    If user has 10 errors, meaning the user failed and the results cannot be included.
    If user has less than 10 errors, user passed because user reached 10 points before reaching 10 errors.

    Args:
        errors_by_user (int): This is the number of errors done by user.
        list_record (list): This is the list of passed users and their results. If user passed, user's results
                     will be appended here.
        current_instance (User()): This is the current user who might have passed or failed.

    Returns:
        Boolean value which indicates whether user has passed or failed
        An updated list of whatever user's result is. If failed, then there are no changes. If passed,
            user's results will be appended here.
    """

    if (errors_by_user == 10):
        result = False
    else:
        result = True
        current_instance.time = timer(start, end)
        counter = 0
        index = 0

        # If current list of players is empty, simply append.
        if not len(list_record):
            list_record.append(current_instance)
        else:                
            for each_player in list_record:
                """
                This makes flag to True if it can find an existing username according to current user's username.
                It also gets the current index of the existing username to be replaced.
                And since it has found an existing, it breaks. (Considering that username should be unique)
                """
                if (each_player.username == current_instance.username):
                    flag = True
                    index = counter
                    break
                else:
                    flag = False
                counter += 1

            """
            This will change the values of existing username if current results have less time or less errors
            if they have the same time. If current username is unique, then it simply appends.
            """
            if flag:
                if (list_record[index].time > current_instance.time):
                    list_record[index].time = current_instance.time
                    list_record[index].errors = current_instance.errors   
                elif (list_record[index].time==current_instance.time
                    and list_record[index].errors>current_instance.errors):
                    list_record[index].errors = current_instance.errors
            else:
                list_record.append(current_instance)

    return result, list_record 


def prompt_result(result):
    """
    This function shows prompts only and instructions for starting/ending the game.

    Args:
        result (Boolean): If True, prompts user that user passed.
    """

    if result:
        print "CONGRATULATIONS! You passed! Want to try a new game?"
    else:
        print "OH NO! You failed. Maybe you want to try again?"

    print """%s
*                                               *
*       To start the game, enter username.      *
*        To terminate, just click enter.        *
*                                               *
%s""" % ("* " * 25, "* " * 25)

def top_10(list_record):
    """
    This function prints the top 10 after the user has clicked enter only. If there are still less than
    10 players who passed, it prints all.

    Args:
        list_record (list): This is the updated and sorted list after the game has terminated.
    """
    if len(list_record) >= 10:
        for index in xrange(10):
            print index + 1, "\t", list_record[index].username, "\t", list_record[index].time, "\t", list_record[index].errors
    
    # This prints all if there are less than 10 currently listed. 
    else:
        for each_player in list_record:
            print index + 1, each_player.username, "\t", each_player.time, "\t", each_player.errors

def update_record(list_record):
    """
    This function simply writes the file with the updated list after the game has terminated.

    Args:
        list_record (list): This is the updated and sorted list after the game has terminated.
    """

    update = open("scores.txt", "w")

    for each_player_updated in list_of_players:
        update.writelines(each_player_updated.time + "\t"
                                + str(each_player_updated.errors) + "\t"
                                + each_player_updated.username + "\n")

    update.close()

list_of_players = get_list_of_players()

print """%sW E L C O M E %s
*                                               *
*     This game will test your math skills!     *
*   Get 10 points to win the game, but if you   *
*    get 10 errors before that, you'll lose.    *
*                                               *
*       To start the game, enter username.      *
*        To terminate, just click enter.        *
*                                               *
%s""" % ("* " * 9, "* " * 9, "* " * 25)

start_game = True

while start_game:
    current_user = User()
    current_user.username = raw_input("Username: ")

    if (current_user.username == ""):
        # This sorts the updated list of players after game is terminated.
        list_of_players = sorted(list_of_players, key = attrgetter('time', 'errors', 'username'))
        top_10(list_of_players)
        start_game = False

    else:
        begin_choice = raw_input("Press enter to begin...")
        start = datetime.now()

        """
        This is where the game starts, the actual parameters are 0 and 0 because initially,
        current user has 0 points and 0 errors.
        """ 
        current_user.errors = questions(0, 0)
        end = datetime.now()

        passed, list_of_players = if_passed(current_user.errors, list_of_players, current_user)
        prompt_result(passed)

print "Game terminated..."
update_record(list_of_players)
